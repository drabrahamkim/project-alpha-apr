from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from .forms import TaskForm
from .models import Task


def some_view_function(request):
    # Do some stuff
    form = PostForm()
    context = {
        # Other data to put in the template
        "form": form,
    }
    return render(request, "dir/template.html", context)


# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(
                "project_list"
            )  # Replace 'project_list' with your project list URL name
    else:
        form = TaskForm()

    return render(request, "tasks/create_task.html", {"form": form})


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    return render(request, "tasks/show_my_tasks.html", {"tasks": tasks})
