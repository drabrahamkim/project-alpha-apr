from django import forms
from .models import Task


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        exclude = [
            "is_completed",
            "owner",
        ]  # Exclude is_completed and owner fields from the form
