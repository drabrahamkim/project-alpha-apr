from django.shortcuts import render, get_object_or_404, redirect
from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from .models import Project
from .forms import ProjectForm


@login_required
def projects_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request):
    projects = Project.objects.all()
    context = {
        "projects": projects,
    }
    return render(request, "projects/detail.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = project.tasks.all()
    return render(
        request,
        "project_detail.html",
        {"project": project, "tasks": tasks},
    )


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("projects:project_list")
    else:
        form = ProjectForm()
    return render(request, "projects/create_project.html", {"form": form})


def project_list_view(request):
    project_list = Project.objects.all()
    context = {"project_list": project_list}
    pass
