from django.urls import include, path
from accounts.views import user_login
from projects.views import show_project, list_projects, login, create_project
from . import views

app_name = "projects"

urlpatterns = [
    path("projects/", show_project, name="show_project"),
    path("", projects_list, name="list_projects"),
    path("login/", user_login, name="login"),
    path("accounts/", include("accounts.urls")),
    path("<int:id>/", views.show_project, name="show_project"),
    path("create/", views.create_project, name="create_project"),
]
